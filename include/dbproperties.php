<?php
/**
* Web based SQLite management
* Show and manage database properties
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: dbproperties.php,v 1.24 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.24 $
*/

foreach(SQLiteFactory::$dbItems as $item){
	$listItem = $workDb->getPropList($item);
	if( $item == 'Table' ) $colspan = 5;
	elseif($item == 'View') $colspan = 3;
	elseif(($item == 'Function') || ($item == 'Trigger')) $colspan=2;

	?>
<center>
<table class="w8">
	<tr>
		<td align="center">
			<fieldset>
				<legend><?php echo Translate::getInstance()->itemTranslated[$item] ?></legend>
<?php if($listItem) : ?>
				<table class="Browse p5 w9">
					<thead>
						<tr>
							<td align="center" class="tabproptitle"><?php echo Translate::getInstance()->itemTranslated[$item]; ?></td>
							<td align="center" colspan="<?php echo $colspan; ?>" class="tabproptitle"><?php echo Translate::g(33); ?></td>
<?php if( ($item == 'Table') || ($item=='View') ) : ?>
							<td align="center" class="tabproptitle" style="white-space: nowrap;"><?php echo Translate::g(118); ?></td>
<?php endif; ?>
						</tr>
					</thead>
<?php
		$totItem = $totEnr = 0;
		foreach($workDb->getPropList($item) as $itemName) {
			$totItem++;
			$linkBase = 'main.php?dbsel='.$GLOBALS['dbsel'].'&amp;'.strtolower($item).'='.$itemName;
			if( ($item == 'Table') || ($item=='View') ) {
				if($workDb->getResId('SELECT count(*) FROM '.brackets($itemName)))
					$nbEnr = $workDb->connId->fetch_single();
				else $nbEnr = '&nbsp;';
					$totEnr+=$nbEnr;
			}
?>
					<tr class="row<?php echo ($totItem%2); ?>">
						<td class="tabprop" align="left">
							<a href="<?php echo $linkBase; ?>" class="PropItemTitle"><span style="font-size: 12px;">&nbsp;<?php echo $itemName; ?></span></a>
						</td>
<?php if(isset($nbEnr) && $nbEnr && ( ($item == 'Table') || ($item=='View') )) : ?>
						<td class="tabprop" align="center"><a href="<?php echo $linkBase; ?>&amp;action=browseItem" class="propItem"><?php echo displayPics("browse2.png", Translate::g(73)); ?></a></td>
<?php elseif( ($item == 'Table') || ($item=='View') ) : ?>
						<td class="tabprop" align="center"><span style="color: gray;"><?php echo displayPics("browse_off.png", Translate::g(73)); ?></span></td>
<?php endif; ?>
<?php if(($item == 'Table')) : ?>
	<?php if(!$workDb->isReadOnly() && displayCondition("data")) : ?>
						<td class="tabprop" align="center"><a href="<?php echo $linkBase; ?>&amp;action=insertElement" class="propItem"><?php echo displayPics("insertrow.png", Translate::g(119)); ?></a></td>
	<?php else : ?>
						<td class="tabprop" align="center"><i><?php echo displayPics("insertrow_off.png", Translate::g(119)); ?></i></td>
	<?php endif; ?>
<?php endif; ?>
						<td class="tabprop" align="center"><a href="<?php echo $linkBase; ?>" class="propItem"><?php echo displayPics("properties.png", Translate::g(61)); ?></a></td>
<?php if(!$workDb->isReadOnly() && displayCondition("del")) :
		$stringConf = Translate::g(120)." ".(($item!="Trigger")? Translate::g(122) : Translate::g(121) )." ".Translate::getInstance()->itemTranslated[$item]." ".$itemName."?";
?>
						<td class="tabprop" align="center">
							<a href="#" onClick="javascript:if(confirm('<?php echo $stringConf; ?>')) parent.main.location='<?php echo $linkBase; ?>&amp;action=delete';" class="propItem">
								<?php echo displayPics("delete_table.png", Translate::g(86)); ?>
							</a>
						</td>
<?php else: ?>
						<td class="tabprop" align="center"><i><?php echo displayPics("delete_table_off.png", Translate::g(86)); ?></i></td>
<?php endif; ?>
<?php if(isset($nbEnr) && $nbEnr && ( $item=="Table" )) : ?>
	<?php if(!$workDb->isReadOnly() && displayCondition("empty")) : ?>
						<td class="tabprop" align="center">
							<a href="#" onClick="javascript:if(confirm('<?php echo Translate::g(123)." ".$itemName; ?>?')) parent.main.location='<?php echo $linkBase; ?>&amp;action=empty';" class="propItem">
								<?php echo displayPics("edittrash.png", Translate::g(77)); ?>
							</a>
						</td>
	<?php else : ?>
						<td class="tabprop" align="center"><i><?php echo displayPics("edittrash_off.png", Translate::g(77)); ?></i></td>
	<?php endif; ?>
<?php elseif( $item=="Table" ) : ?>
						<td class="tabprop" align="center"><span style="color: gray;"><?php echo displayPics("edittrash_off.png", Translate::g(77)); ?></span></td>
<?php endif; ?>
<?php if( ($item == 'Table') || ($item=='View') ) : ?>
						<td class="tabprop" align="center"><?php echo $nbEnr; ?></td>
<?php endif; ?>
					</tr>
<?php } ?>
					<tr>
						<td align="center" class="tabproptitle" style="white-space: nowrap;"><?php echo $totItem.' '.Translate::getInstance()->itemTranslated[$item].Translate::g(228); ?></td>
						<td colspan="<?php echo $colspan; ?>" class="tabproptitle">&nbsp;</td>
<?php if( ($item == 'Table') || ($item=='View') ) : ?>
						<td align="center" class="tabproptitle"><?php echo $totEnr; ?></td>
<?php endif; ?>
					</tr>
				</table>
<?php endif;  ?>
<?php if(!$workDb->isReadOnly() && displayCondition('properties')) :
		$baseLink = 'main.php?dbsel=' . Request::getInt('dbsel') . '&amp;action=';
		switch($item){
			case 'Table':
?>
				<form name="add<?php echo $type; ?>" action="main.php" method="POST" target="main">
					<span style="font-size: 12px;">
						<?php echo Translate::g(43); ?> ==&gt;&nbsp;<?php echo Translate::g(19); ?>&nbsp;:&nbsp;
						<input type="text" name="TableName" size="20" class="small-input">&nbsp;-&nbsp;
						<input type="text" name="nbChamps" size="3" class="small-input">&nbsp;<?php echo Translate::g(44); ?>&nbsp;
						<input class="button" type="submit" value="<?php echo Translate::g(69); ?>">
						<input type="hidden" name="dbsel" value="<?php echo Request::getInt('dbsel'); ?>">
						<input type="hidden" name="action" value="add_<?php echo strtolower($type); ?>">
					</span>
				</form>
<?php
				break;
			case 'View':
?>
				<a href="<?php echo $baseLink; ?>add_view" class="propItemTitle" target="main">&nbsp;&raquo;&nbsp;<?php echo Translate::g(87); ?></a>
<?php
				break;
			case 'Trigger':
?>
				<a href="<?php echo $baseLink; ?>add_trigger" class="propItemTitle" target="main">&nbsp;&raquo;&nbsp;<?php echo Translate::g(88); ?></a>
<?php
				break;
			case 'Function':
?>
				<a href="<?php echo $baseLink; ?>add_function" class="propItemTitle" target="main">&nbsp;&raquo;&nbsp;<?php echo Translate::g(89); ?></a>
<?php
				break;
		}
?>
<?php endif; ?>
			</fieldset>
		</td>
	</tr>
</table>
</center>
<?php
}
?>
</body>
</html>
