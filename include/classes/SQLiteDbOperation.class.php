<?php
/**
* Web based SQLite management
* Class for manage database operation
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: SQLiteDbOperation.class.php,v 1.9 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.9 $
*/
class SQLiteDbOperation {

	/**
	* reference to the connection object
	*
	* @access public
	* @var object
	*/
	var $connId;

	/**
	 *
	 * @access private
	 * @var array
	 */
	var $tabDb;

	/**
	* Class constructor
	*
	* @access public
	* @param string $conn reference to the connection object
	*/
	function __construct(&$connId){
		$this->connId = $connId;
		$error = false;
		$dbSel = Request::getInt('dbsel');
		$workDb = SQLiteDbConnect::getSelected();
		$table = Request::getCmd('table');

		$operation_action = Request::getWord('operation_action', '');

		$tabInfoDb = $this->getTabDb();
		foreach($tabInfoDb as $trash=>$locDb)
			$locTabInfoDb[$locDb["id"]] = $locDb["name"];

		switch($operation_action) {
			case '':
				$this->operationView();
				break;
			case 'renameTable':
				$newTable = Request::getCmd('newName');
				$error = $workDb->copyTable($table, brackets($newTable, false), false);
				if($error)
					$this->operationView();
				break;
			case 'moveTable':
				$dbDest = Request::getInt('dbDest');
				$moveName = Request::getCmd('moveName');
				if($dbDest == $GLOBALS["tabInfoDb"]["id"])
				    $destTableInfo = brackets($moveName);
				else
				    $destTableInfo = brackets($locTabInfoDb[$dbDest], false).".".brackets($moveName, false);

				$error = $workDb->copyTable($table, $destTableInfo, false);
				if($error)
					$this->operationView();
				break;
			case 'copyTable':
				$dbDest = Request::getInt('dbDest');
				$copyName = Request::getCmd('copyName');
				if($dbDest == $GLOBALS["tabInfoDb"]["id"])
				    $destTableInfo = brackets($copyName, false);
				else
				    $destTableInfo = brackets($locTabInfoDb[$dbDest], false).".".brackets($copyName, false);

				$error = $workDb->copyTable($table, $destTableInfo, true);
				if($error)
					$this->operationView();
				break;
		}

		if($operation_action && !$error){
			$GLOBALS['redirect'] = <<<EOT
<script type="text/javascript">
parent.left.location='left.php?dbsel=$dbSel';
parent.main.location='main.php?dbsel=$dbSel';
</script>
EOT;
		}
	}

	/**
	* Display Available Operation
	*
	* @access public
	*/
	function operationView(){
		$dbSel = Request::getInt('dbsel');
		$table = Request::getCmd('table');
		$action = Request::getWord('action');
?>
		<table class="w8 center">
			<tr>
				<td>
					<div>
						<form name="Rename" action="main.php" method="post" target="main">
							<input type="hidden" name="dbsel" value="<?php echo $dbSel; ?>" />
							<input type="hidden" name="table" value="<?php echo $table; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							<input type="hidden" name="operation_action" value="renameTable" />
							<table class="Browse p5" width="100%">
								<thead>
									<tr class="Browse">
										<td colspan=2 align="left" class="tapPropTitle"><?php echo Translate::g(215); ?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="text" name="newName" size="40"></td>
										<td align="right"><input type="submit" value="<?php echo Translate::g(69); ?>" class="button"></td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<form name="Move" action="main.php" method="post" target="main">
							<input type="hidden" name="dbsel" value="<?php echo $dbSel; ?>" />
							<input type="hidden" name="table" value="<?php echo $table; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							<input type="hidden" name="operation_action" value="moveTable" />
							<table class="Browse p5" width="100%">
								<thead>
									<tr class="Browse">
										<td colspan=2 align="left" class="tapPropTitle"><?php echo Translate::g(216); ?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<?php echo $this->getDbList(); ?>.<input type="text" name="moveName" size="40">
										</td>
										<td align="right">
											<input type="submit" value="<?php echo Translate::g(69); ?>" class="button">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<form name="Copy" action="main.php" method="post" target="main">
							<input type="hidden" name="dbsel" value="<?php echo $dbSel; ?>" />
							<input type="hidden" name="table" value="<?php echo $table; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							<input type="hidden" name="operation_action" value="copyTable" />
							<table class="Browse p5" width="100%">
								<thead>
									<tr class="Browse">
										<td colspan=3 align="left" class="tapPropTitle"><?php echo Translate::g(217); ?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<?php echo $this->getDbList($dbSel); ?>.<input type="text" name="copyName" size="40">
										</td>
										<td style="white-space: nowrap;">
											<input name="whatToDo" value="structure" style="vertical-align: middle;" type="radio">
											<label><?php echo Translate::g(124); ?></label>
											<br>
											<input name="whatToDo" value="data" checked="checked" style="vertical-align: middle;" type="radio">
											<label><?php echo Translate::g(125); ?></label>
											<br>
											<input name="whatToDo" value="dataonly" style="vertical-align: middle;" type="radio">
											<label><?php echo Translate::g(126); ?></label>
											<br>
											<input name="dropTable" value="true" style="vertical-align: middle;" type="checkbox">
											<label><?php echo Translate::g(218); ?></label>
											<br>
										</td>
										<td align="right">
											<input type="submit" value="<?php echo Translate::g(69); ?>" class="button">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</td>
			</tr>
		</table>
<?php
	}

	/**
	* Display SQlite error
	*
	* @access public
	* @param string $queryDisplay Query to display
	* @param resource $res Database connection resource
	*/
	function _getError($queryDisplay, $res){
		if(!$res){
			$errorMessage .= Translate::g(9).' : '.@$this->connId->connId->getError()."\n";
		}
		displayQuery($queryDisplay);
		if(!empty($errorMessage)) displayError($errorMessage);

	}

	function getDbList($selected=""){
		$tabInfoDb = $this->getTabDb();
		$dbList = '<select name="dbDest">';
		foreach($tabInfoDb as $dbInfo) $dbList .= '<option value="'.$dbInfo["id"].'"'.(($dbInfo["id"]==$selected)? ' selected' : '' ).'>'.$dbInfo["name"].'</option>';
		$dbList .= '</select>';
		return $dbList;
	}

	function getTabDb(){
		$this->tabDb = array();
		$tempTabDb = $GLOBALS["db"]->array_query("SELECT id, name, location FROM database", SQLITE_ASSOC);
		foreach($tempTabDb as $tabDbInfo) {
			if(sqlite::getDbVersion($tabDbInfo['location']) == $this->connId->connId->getVersion()) {
				$this->tabDb[] = $tabDbInfo;
			}
		}
		return $this->tabDb;
	}
}
?>