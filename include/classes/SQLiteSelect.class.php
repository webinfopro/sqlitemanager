<?php
/**
* Web based SQLite management
* Class for generate 'Select' querys
* @package SQLiteManager
* @author Maurício M. Maia <mauricio.maia@gmail.com>
* @version $Id: SQLiteSelect.class.php,v 1.18 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.18 $
*/

class SQLiteSelect {

	/**
	* reference to the connection object
	*
	* @access public
	* @var object
	*/
	var $connId;

	/**
	* TABLE name
	*
	* @access private
	* @var string
	*/
	var $table;


	/**
	* Class constructor
	*
	* @access public
	* @param string $conn reference to the connection object
	*/
	function __construct(&$conn){
		// constructeur de la classe
		$this->connId = $conn;

		$table = Request::getCmd('table');
		$tableName = Request::getCmd('TableName');
		$view = Request::getCmd('view');
		$viewName = Request::getCmd('ViewName');
		$function = Request::getCmd('function');
		$selectAction = Request::getWord('select_action');

		if($table) {
			$this->table = $table;
		} elseif($tableName){
			$this->table = $tableName;
		} else if($view) {
			$this->table = $view;
		} elseif($viewName){
			$this->table = $viewName;
		} else
			return false;

		switch($selectAction){
			case '':
				$this->getTableInfo($this->table);
				$this->form();
				break;
		}
	}

	/**
	* Get some table properties
	*
	* @access public
	* @param string $table table name
	*/
	function getTableInfo($table=''){
		if(empty($table))
			$table = $this->table;

		$this->connId->getResId('PRAGMA table_info('.brackets($table).');');
		$this->infoTable = $this->connId->getArray();

		return $this->infoTable;
	}

	/**
	* Display Form for select table records
	*
	* @access public
	* @param string $req query where the record is
	* @param integer $numId Number of the query record
	* @param boolean $error if true, display POST value
	*/
	function form(){
?>
		<!-- SQLiteSelect.class.php : form() -->
		<div style="text-align: center;">
			<h4><?php echo Translate::g(201); ?></h4>
			<form name="select" action="main.php?dbsel=<?php echo Request::getWord('dbsel'); ?>&amp;table=<?php echo $this->table; ?>" method="post" target="main">
				<table class="Insert p5 center w8">
					<thead>
						<tr>
							<td align="center" class="Browse"><?php echo Translate::g(73); ?></td>
							<td align="center" class="Browse"><?php echo Translate::g(27); ?></td>
							<td align="center" class="Browse"><?php echo Translate::g(28); ?></td>
							<td align="center" class="Browse"><?php echo Translate::g(202); ?></td>
							<td align="center" class="Browse"><?php echo Translate::g(50); ?></td>
						</tr>
					</thead>
					<tbody>
<?php while(list($cid, $tabInfo) = each($this->infoTable)) {
?>
						<tr>
							<td align="center" class="Insert">
								<input type="checkbox" name="showField[<?php echo $tabInfo['name']; ?>]" checked="checked" />
							</td>
							<td align="left" class="Insert"><?php echo $tabInfo['name']; ?></td>
							<td align="center" class="Insert"><?php echo strtoupper($tabInfo['type']); ?></td>
							<td align="center" class="Insert">
								<select name="operats[<?php echo $tabInfo['name'];?>]">
									<option value="" />
							<?php
							foreach(ParsingQuery::$SQLselect as $operat) {
								if(($operat != "fulltextsearch") || ALLOW_FULLSEARCH) {
							?>
									<option value="<?php echo $operat; ?>"><?php echo $operat; ?></option>
							<?php
								}
							}
							?>
								</select>
							</td>
							<td align="left" class="Insert">
							<?php
							echo SQLiteInputType($infoTable, '', false, false);
							?>
							</td>
						</tr>
<?php } ?>
						<tr>
							<td style="text-align:left" colspan="10">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo displayPics('arrow_ltr.gif'); ?>&nbsp;
								<a href="#" onClick="javascript:setCheckBox('select','showField',true);" class="Browse"><?php echo Translate::g(34); ?></a>
								&nbsp;/&nbsp;
								<a href="#" onClick="javascript:setCheckBox('select', 'showField', false);" class="Browse"><?php echo Translate::g(35); ?></a>
							</td>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td class="TitleHeader" style="text-align:left"><?php echo Translate::g(203); ?></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td style="text-align:right; white-space: nowrap">
								<label for="AND"><?php echo Translate::g(204); ?></label>
								<input type="radio" name="operSuppl" id="AND" value="AND" checked="checked" />
								<br/>
								<label for="OR"><?php echo Translate::g(205); ?></label>
								<input type="radio" name="operSuppl" id="OR" value="OR" />
							</td>
							<td style="text-align:left">
								<textarea name="CondSuppl" cols="<?php echo TEXTAREA_NB_COLS; ?>" rows="4"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
<?php if(isset($req)) : ?>
				<input type="hidden" name="numId" value="<?php echo $numId; ?>" />
				<input type="hidden" name="req" value="<?php echo urlencode($req); ?>" />
<?php endif; ?>
				<input type="hidden" name="action" value="selectElement" />
<?php if(isset($_REQUEST['currentPage'])) : ?>
				<input type="hidden" name="currentPage" value="<?php echo Request::getInt('currentPage'); ?>" />
<?php endif; ?>
				<input class="button" type="submit" value="<?php echo Translate::g(201); ?>" onclick="document.tabprop.submit();">
			</form>
		</div>
<?php
	}
}
?>