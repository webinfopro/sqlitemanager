<?php
/**
* Web based SQLite management
* Class for database structure export
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: SQLiteExport.class.php,v 1.27 2006/04/18 09:19:20 freddy78 Exp $ $Revision: 1.27 $
*/

class SQLiteExport {

	/**
	* reference to the connection object
	*
	* @access public
	* @var object
	*/
	var $connId;

	/**
	* Export content
	*
	* @access private
	* @var array
	*/
	var $exportContent;

	/**
	* User Defined Function properties
	*
	* @access private
	* @param array $UDFprop
	*/
	var $UDFprop;

	/**
	 *
	 * Full table's information
	 *
	 * @access private
	 * @param array $tableProperties
	 */
	var $tableProperties;

	/**
	 *
	 * Trigger information
	 *
	 * @access private
	 * @param array $triggerProperties
	 */
	var $triggerProperties;

	/**
	* Class constructor
	*
	* @access public
	* @param string $conn reference to the connection object
	*/
	function __construct(&$connId){
		$this->connId = $connId;
		$exportAction = Request::getWord('export_action', '');
		switch($exportAction){
			case '':
				$this->form();
				break;
			case 'go':
				$this->exportContent = '';
				$table = Request::getCmd('table');
				$view = Request::getCmd('view');
				$function = Request::getCmd('function');
				$type = Request::getInt('type');

				$this->exportHeader();

				if($table){
					$this->exportTableSelector($table, $type);
				} elseif($view) {
					$this->viewProperties($view);
				} elseif($function) {
					$this->functionProperties($function);
				} else {
					$this->dbProperties();
				}

				$this->send();
				break;
		}
	}

	/**
	* Display form for option choose
	*
	* @access public
	*/
	function form(){
		$queryExport = Request::getString('queryExport');
		$table = Request::getCmd('table');
		$view = Request::getCmd('view');
		$function = Request::getCmd('function');
		if($queryExport) {
			$query = urldecode(SQLiteStripSlashes($queryExport));
			displayQuery($query, false);
		}
?>
		<form name="export" action="main.php" method="post" target="main">
			<table class="Insert center w5" style="text-align: left;">
				<tr>
					<td valign="top">
						<table class="w10">
							<thead>
								<tr>
									<td class="tabproptitle"><?php echo Translate::g(76); ?></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding-left:15px;padding-top:15px;">
										<input type="radio" name="type" id="type1" value="1" checked="checked" />
										<label for="type1"><?php echo Translate::g(124); ?></label>
										<br/>
										<input type="radio" name="type" id="type2" value="2" />
										<label for="type2"><?php echo Translate::g(125); ?></label>
										<br/>
										<input type="radio" name="type" id="type3" value="3" />
										<label for="type3"><?php echo Translate::g(126); ?></label>
										<br/>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						<table class="w10">
							<thead>
								<tr>
									<td class="tabproptitle"><?php echo Translate::g(72); ?>&nbsp;:</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<input type="checkbox" name="drop" id="drop1" value="1" />
										<label for="drop1"><?php echo Translate::g(43); ?>&nbsp;"DROP"</label>
										<br/>
										<br/>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="w10">
							<thead>
								<tr>
									<td class="tabproptitle"><?php echo Translate::g(199); ?>&nbsp;:</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<input type="checkbox" name="fullInsert" id="fullInsert1" value="1" />
										<label for="fullInsert1"><?php echo Translate::g(127); ?></label>
										<br/>
										<br/>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="checkbox" name="trans" id="trans1" value="1" />
						<label for="trans1"><?php echo Translate::g(128); ?></label>
						<input type="checkbox" name="win" id="win1" value="1" />
						<label for="win1">CRLF</label>
					</td>
				</tr>
				<tr>
					<td align="center" style="padding: 3px;" colspan="2"><input class="button" type="submit" value="<?php echo Translate::g(69); ?>"></td>
				</tr>
			</table>
			<input type="hidden" name="dbsel" value="<?php echo Request::getInt('dbsel'); ?>" />
<?php if($table) : ?>
			<input type="hidden" name="table" value="<?php echo $table; ?>" />
<?php endif; ?>
<?php if($queryExport) : ?>
			<input type="hidden" name="queryExport" value="<?php echo SQLiteStripSlashes($queryExport); ?>" />
<?php endif; ?>
<?php if($view) : ?>
			<input type="hidden" name="view" value="<?php echo $view; ?>" />
<?php endif; ?>
<?php if($function) : ?>
			<input type="hidden" name="function" value="<?php echo $function; ?>" />
<?php endif; ?>
			<input type="hidden" name="action" value="<?php echo Request::getWord('action'); ?>" />
			<input type="hidden" name="export_action" value="go" />
		</form>
<?php
	}

	/**
	* return formated header
	*
	* @access public
	*/
	function exportHeader(){
		$out =  '# SQLiteManager Dump#%BREAK%#';
		$out .= '# Version: '.SQLiteManagerVersion.'#%BREAK%#';
		$out .= '# http://www.sqlitemanager.org/#%BREAK%#';
		$out .= '# #%BREAK%#';
		$out .= '# '.Translate::gdirect(129).': '.$_SERVER['HTTP_HOST'].'#%BREAK%#';
		$out .= '# '.Translate::gdirect(130).': '. date('l dS of F Y h:i a').'#%BREAK%#';
		$out .= '# SQLite Version: '.@$this->connId->connId->sqlite_version().'#%BREAK%#';
		$out .= '# PHP Version: '.@phpversion().'#%BREAK%#';
		$out .= '# '.Translate::gdirect(131).': '.basename($this->connId->baseName).'#%BREAK%#';
		$out .= '# --------------------------------------------------------#%BREAK%#';
		$this->exportContent .= $out;
		return $out;
	}

	/**
	* send content with correct header
	*
	* @access public
	*/
	function send(){
		if(!isset($_POST['trans']) || empty($_POST['trans'])){
			$tabOut = explode('#%BREAK%#', $this->exportContent);
			echo '<table class="export"><tr><td style="white-space: nowrap">';
			foreach($tabOut as $ligneOut) echo htmlentities($ligneOut, ENT_NOQUOTES, Translate::encoding()).'<br>';
			echo '</td></tr></table>';
		} else {
			// envoi du fichier
			ob_end_clean();
			if(isset($_POST['table']) && !empty($_POST['table'])) $filename = $_POST['table'];
			else $filename = basename($this->connId->baseName);
			header('Content-Type: application/download');
			header('Content-Disposition: attachment; filename='.$filename.'.sql');
			if(!isset($_POST['win']) || empty($_POST['win']))
				echo str_replace('#%BREAK%#', "\n", $this->exportContent);
			else
				echo str_replace('#%BREAK%#', "\r\n", $this->exportContent);
		}
	}

	/**
	* return table properties
	*
	* @access public
	* @param string $table table name
	*/
	function tableProperties($table){
		$this->getTableProperties($table);
		$out =  '#%BREAK%###%BREAK%#';
		$out .= '# '.Translate::gdirect(132).': '.$table.'#%BREAK%#';
		$out .= '##%BREAK%#';
		$query = 'SELECT sql, type FROM sqlite_master WHERE sql IS NOT NULL AND tbl_name='.quotes($table).';';
		$this->connId->getResId($query);
		if(isset($_POST['drop']) && !empty($_POST['drop'])) $out .= 'DROP TABLE '.brackets($table).';#%BREAK%#';
		$tabResult = $this->connId->getArray();
		foreach($tabResult as $sqlProp) {
			if($sqlProp['type'] != 'trigger') {
				$out .= $sqlProp['sql'].';#%BREAK%#';
			} else {
				$this->triggerProperties[] = $sqlProp['sql'];
			}
		}
		$this->exportContent .= $out;
		return;
	}

	/**
	* return table data
	*
	* @access public
	* @param string $table table name
	*/
	function tableContent($table){
		$out =  '#%BREAK%###%BREAK%#';
		$out .= '# '.Translate::gdirect(133).': '.$table.'#%BREAK%#';
		$out .= '##%BREAK%#';
		// build nullByName array
		if(!is_array($this->tableProperties)) $this->getTableProperties($table);
		if(is_array($this->tableProperties)){
			foreach($this->tableProperties as $trash=>$tabInfoTable){
				$nullByName[$tabInfoTable["name"]] = $tabInfoTable["notnull"];
			}
		} else {
			$nullByName = array();
		}

		if(isset($_REQUEST['queryExport']) && $_REQUEST['queryExport']) $query = urldecode(SQLiteStripSlashes($_REQUEST['queryExport']));
		else $query = 'SELECT * FROM '.brackets($table);
		$this->connId->connId->query($query);
		while($ligne = $this->connId->connId->fetch_array(null, SQLITE_ASSOC)) {
			if(isset($_POST['fullInsert']) && !empty($_POST['fullInsert']) && !isset($columnList)){
				for($i=0 ; $i<$this->connId->connId->num_fields() ; $i++) {
					$currentNameField = $this->connId->connId->field_name(null, $i);
					$columnList[$i] = brackets($currentNameField);
				}
			}
			$columnValue = array();
			$out .= 'INSERT INTO '.brackets($table);
			if(isset($_POST['fullInsert']) && !empty($_POST['fullInsert'])) {
				$out .= ' ('.implode(', ', $columnList).')';
			}
			while(list($key, $val) = each($ligne)) {
				$columnValue[$key] = "'".$this->connId->formatString($val)."'";
				if(isset($nullByName[$key]) && !$nullByName[$key] && ($columnValue[$key] == "''")) $columnValue[$key] = "NULL";
			}
			$out .= " VALUES (".implode(", ", $columnValue).");#%BREAK%#";
		}
		$out = str_replace(";\r\n", "; ", $out);
		$this->exportContent .= $out;
		return;
	}

	/**
	* Iterator for export all database
	*
	* @access private
	*/
	function dbProperties(){
		$tableList = $this->connId->getPropList('Table');
		if(is_array($tableList)) foreach($tableList as $tableName) $this->exportTableSelector($tableName, $_POST['type']);
		if(count($this->triggerProperties)) {
			// Export Trigger after Table Properties
			$this->exportContent .=  '#%BREAK%###%BREAK%#';
			$this->exportContent .= '# '.Translate::gdirect(233).'#%BREAK%#';
			$this->exportContent .= '##%BREAK%#';
			$this->exportContent .= implode(';#%BREAK%#', $this->triggerProperties);
			$this->exportContent .= ';#%BREAK%#';
			$this->exportContent .= '# --------------------------------------------------------#%BREAK%##%BREAK%#';
		}
		$viewList = $this->connId->getPropList('View');
		if(is_array($viewList)) foreach($viewList as $viewName) $this->viewProperties($viewName);
		$functionList = $this->connId->getPropList('Function');
		if(is_array($functionList)) foreach($functionList as $functionName) $this->functionProperties($functionName);
		return;
	}

	/**
	* Selector for export table
	*
	* @access private
	* @param string $table table name
	* @param int $type type for controle the output
	*/
	function exportTableSelector($table, $type){
		switch($type){
			case 1:
				$this->tableProperties($table);
				break;
			case 2:
				$this->tableProperties($table);
				$this->tableContent($table);
				break;
			case 3:
				$this->tableContent($table);
				break;
		}
		$this->exportContent .= '# --------------------------------------------------------#%BREAK%##%BREAK%#';
		return;
	}

	/**
	* return View properties
	*
	* @access public
	* @param string $table table name
	*/
	function viewProperties($view){
		$out =  '#%BREAK%###%BREAK%#';
		$out .= '# '.Translate::gdirect(134).': '.$view.'#%BREAK%#';
		$out .= '##%BREAK%#';
		$query = "SELECT sql FROM sqlite_master WHERE type='view' AND tbl_name=".quotes($view)." ORDER BY rootpage";
		$this->connId->getResId($query);
		if(isset($_POST['drop']) && $_POST['drop']) $out .= 'DROP VIEW '.brackets($table).';#%BREAK%#';
		foreach($this->connId->getArray() as $sqlProp) {
			$out .= $sqlProp['sql'].';#%BREAK%#';
		}
		$this->exportContent .= $out;
		return;
	}

	/**
	* return function properties
	*
	* @access public
	* @param string $table table name
	*/
	function functionProperties($function){
		$out =  '#%BREAK%###%BREAK%#';
		$out .= '# '.Translate::gdirect(135).': '.$function.'#%BREAK%#';
		$out .= '##%BREAK%#';
		if(!is_array($this->UDFprop)) $this->UDFprop = $this->connId->getUDF();
		foreach($this->UDFprop as $UDF) {
			if($UDF['funct_name'] == $function){
				$propFunct = $UDF;
				break;
			}
		}
		if(is_array($propFunct)){
			$out .= '/*#%BREAK%#';
			$out .= $propFunct['funct_code'];
			if($propFunct['funct_type']==2) $out .= $propFunct['funct_final_code'].'#%BREAK%#';
			$out .= '#%BREAK%#*/#%BREAK%#';
		}
		$this->exportContent .= $out;
		return;
	}

	function getTableProperties($table){
		$O_TableProperties = new SQLiteTableProperties($this->connId, $table);
		$this->tableProperties = $O_TableProperties->getTableProperties();
		return $this->tableProperties;
	}

}
?>
