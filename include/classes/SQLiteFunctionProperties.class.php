<?php
/**
* Web based SQLite management
* FUNCTION management Class
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: SQLiteFunctionProperties.class.php,v 1.34 2006/04/16 18:56:57 freddy78 Exp $ $Revision: 1.34 $
*/

class SQLiteFunctionProperties {

	/**
	* reference to the connection object
	*
	* @access public
	* @var resource
	*/
	var $connId;

	/**
	* function name
	*
	* @access private
	* @var string
	*/
	var $function;

	/**
	* this function exist?
	*
	* @access private
	* @var bool
	*/
	var $isExist;

	/**
	* Properties of the current FUNCTION
	*
	* @access private
	* @var array
	*/
	var $functionProperties;

	/**
	* Class constructor
	*
	* @access public
	* @param object $conn reference to the connection object
	*/
	function __construct(&$conn){
		// constructeur de la classe
		$this->connId = $conn;
		$function = Request::getCmd('function');
		$functionName = Request::getCmd('FunctionName');
		$action = Request::getWord('action');
		if($function && $action!='add')
			$this->function = $function;
		elseif($functionName)
			$this->function = $functionName;
		else
			return false;

		$this->isExist = $this->functionExist($this->function);

		return $this->isExist;
	}

	/**
	* Verify if this FUNCITION exist
	*
	* @access public
	* @param string
	*/
	function functionExist($function){
		$db = SQLiteFactory::sqliteGetInstance(SQLiteDb);

		if(empty($function))
			$function = $this->function;

		$query = 'SELECT * FROM user_function WHERE funct_name='.quotes($function).' AND (base_id='.Request::getInt('dbsel').' OR base_id IS NULL);';
		$tempTabFunction = $db->array_query($query);
		if(count($tempTabFunction)==1){
			$exist = false;
			foreach($tempTabFunction as $tempFunctProp) {
				$this->functionProperties = $tempFunctProp;
				$exist = true;
			}
			return $exist;
		} else return false;
	}

	/**
	* save properties of the current FUNCTION
	*
	* @access private
	*/
	function saveProp(){
		$action = Request::getWord('action');
		$db = SQLiteFactory::sqliteGetInstance(SQLiteDb);

		if($action == 'delete') {
			$queryDisplay = 'DELETE FROM user_function WHERE funct_name='.quotes($this->function).' AND (base_id='.Request::getInt('dbsel').' OR base_id IS NULL);';
		} else {
			$base_id = ((Request::getInt('FunctAttribAll')==1)? 'NULL' : Request::getInt('dbsel') );
			$functionName = Request::getCmd('FunctName');
			$FunctType = Request::getInt('FunctType');
			$FunctCode = Request::getRaw('FunctCode');
			$FunctFinalCode = Request::getRaw('FunctFinalCode');
			$FunctNumArgs = Request::getInt('FunctNumArgs');

			if($FunctName != $this->functionProperties['funct_name'])
				$tabSQL['funct_name'] = "'".$this->connId->formatString($FunctName)."'";
			if($FunctType != $this->functionProperties['funct_type'])
				$tabSQL['funct_type'] = $this->connId->formatString($FunctType);
			if($FunctCode != $this->functionProperties['funct_code'])
				$tabSQL['funct_code'] = "'".$this->connId->formatString($FunctCode)."'";
			if($FunctFinalCode != $this->functionProperties['funct_final_code'])
				$tabSQL['funct_final_code'] = "'".$this->connId->formatString($FunctFinalCode)."'";
			if($FunctNumArgs != $this->functionProperties['funct_num_args'])
				$tabSQL['funct_num_args'] = $this->connId->formatString($FunctNumArgs);

			if($base_id	!= $this->functionProperties['base_id'])
				$tabSQL['base_id'] = $base_id;

			if(is_array($tabSQL)){
				if($this->isExist) {
					while(list($key, $value) = each($tabSQL))
						$tabUpdate[] = $key.'='.$value;
					$queryDisplay = 'UPDATE user_function SET '.implode(',', $tabUpdate).' WHERE id='.Request::getInt('id').';';
				} else {
					$tabCol = array_keys($tabSQL);
					$tabVal = array_values($tabSQL);
					$nbVal = count($tabSQL);
					$queryDisplay = 'INSERT INTO user_function ('.implode(',', $tabCol).') VALUES ('.implode(',', $tabVal).');';
				}
			}
		}

		$errorMessage = '';
		$res = $db->query($queryDisplay);
		if(!$res){
			$errorCode = @sqlitem_last_error($this->connId->connId);
			$errorMessage .= Translate::g(9).' '.$errorCode.' : '.@$this->connId->connId->getError()."\n";
		}
		displayQuery($queryDisplay);

		if(!empty($errorMessage))
			displayError($errorMessage);

		if($action != 'delete') {
			$this->propView();
			$GLOBALS['redirect'] = "<script  type=\"text/javascript\">parent.left.location='left.php?dbsel=".$GLOBALS["dbsel"]."';</script>";
		} else {
			$GLOBALS['redirect'] = "<script  type=\"text/javascript\">parent.left.location='left.php?dbsel=".$GLOBALS["dbsel"]."'; parent.main.location='main.php?dbsel=".$GLOBALS["dbsel"]."';</script>";
		}
	}

	/**
	* Display current FUNCITION properties
	*
	* @access public
	*/
	function propView(){
	  echo '<!-- SQLiteFunctionProperties.class.php : propView() -->'."\n";
		echo '<br><center>';
		$funct_code = highlight_string("<?php\n".$this->functionProperties['funct_code']."\n?>", true);
		$funct_finale_code = highlight_string($this->functionProperties['funct_final_code'], true);
		echo '	<table class="viewProp p5 w8">
					<tr class="viewPropTitle"><td align="right" width="20%" class="viewPropTitle">'.Translate::g(19).' :&nbsp;</td><td align="center" class="viewPropTitle">'.htmlentities($this->function, ENT_NOQUOTES, Translate::encoding()).'</td></tr>
					<tr><td align="right" class="viewProp">Type :&nbsp;</td><td align="center" class="viewProp">'.(($this->functionProperties['funct_type']==1)? Translate::g(10) : Translate::g(11) ).'</td></tr>
					<tr><td align="right" class="viewProp">'.Translate::g(10).' :&nbsp;</td><td class="viewProp">'.$funct_code.'</td></tr>';
		if($this->functionProperties['funct_type']==2) echo '		<tr><td align="right" class="viewProp">'.Translate::g(12).' :&nbsp;</td><td class="viewProp">'.$funct_final_code.'</td></tr>';
		echo '			<tr><td align="right" class="viewProp">'.Translate::g(13).' :&nbsp;</td><td class="viewProp">'.$this->functionProperties['funct_num_args'].'</td></tr>';
		echo '		</table>';
		echo '<div align="center">';
		if(!$GLOBALS['workDb']->isReadOnly() && displayCondition('properties')) echo '<a href="main.php?dbsel='.$GLOBALS['dbsel'].'&amp;function='.$this->function.'&amp;action=modify" class="base" target="main">'.Translate::g(14).'</a>';
		else echo '<span class="base"><i>'.Translate::g(14).'</i></span>';
		echo str_repeat('&nbsp;', 10);
		if(!$GLOBALS['workDb']->isReadOnly() && displayCondition('del')) echo '<a href="main.php?dbsel='.$GLOBALS['dbsel'].'&amp;function='.$this->function.'&amp;action=delete" class="base" target="main">'.Translate::g(15).'</a>';
		else echo '<span class="base"><i>'.Translate::g(15).'</i></span>';
		echo '</div>';
		echo '</center>';
}

	/**
	* Display FUNCTION add or modify Form
	*
	* @access public
	*/
	function functEditForm(){
		if($this->isExist){
			$FunctName = $this->function;
			$FunctProp = $this->functionProperties;
			$attribAll = (($FunctProp['base_id']=='')? 1 : 0 );
		} else {
			$FunctName = '';
			$FunctProp = array('id'=>false, 'funct_type'=>1, 'funct_code'=>'', 'funct_final_code'=>'', 'funct_num_args'=>0);
			$attribAll = 0;
		}
?>
		<!-- SQLiteFunctionProperties.class.php : functEditForm() -->
		<h4><?php if(Request::getWord('action') == 'add') : ?><?php echo Translate::g(16); ?><?php else : ?><?php echo Translate::g(17).' : '.$this->function; ?><?php endif; ?></h4>
		<script type="text/javascript">
		function subform(){
			base=document.forms['functprop'];
			error=false;
			if(base.elements['FunctName'].value=='') error=true;
			if(base.elements['FunctCode'].value=='') error=true;
			if(base.elements['FunctNumArgs'].value=='') error=true;
			if( (base.elements['FunctType'].selectedIndex==1) && (base.elements['FunctFinalCode'].value=='') ) error=true;
			if(!error){
				if(base.elements['function'].value=='')
					base.elements['function'].value=base.FunctName.value;
				return true;
			} else {
				alert('<?php echo html_entity_decode(Translate::g(18), ENT_NOQUOTES, Translate::encoding()); ?>');
				return false;
			}
		}
		</script>";
		<form name="functprop" action="main.php?dbsel=<?php echo Request::getInt('dbsel'); ?>" method="post" onSubmit="return subform();" target="main">';
			<table class="p5 w8">
				<tr>
					<td align="right" class="viewPropTitle"><?php echo Translate::g(19); ?>&nbsp;:</td>
					<td class="viewProp"><input type="text" class="text" name="FunctName" value="<?php echo $FunctName; ?>" /></td>
				</tr>
				<tr>
					<td align="right" class="viewPropTitle"><?php echo Translate::g(20); ?>&nbsp;:</td>
					<td class="viewProp">
						<select name="FunctType" onChange="ftype();">
							<option value="1"<?php if($FunctProp['funct_type']==1) : ?> selected="selected"<?php endif; ?>><?php echo Translate::g(10); ?></option>
							<option value="2"<?php if($FunctProp['funct_type']==2) : ?> selected="selected"<?php endif; ?>><?php echo Translate::g(11); ?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td align="right" class="viewPropTitle"><?php echo Translate::g(21); ?>&nbsp;:</td>
					<td class="viewProp">
						<textarea name="FunctCode" cols="<?php echo TEXTAREA_NB_COLS; ?>" rows="<?php echo TEXAREA_NB_ROWS; ?>"><?php echo htmlentities($FunctProp['funct_code'], ENT_NOQUOTES, Translate::encoding()); ?></textarea>
					</td>
				</tr>
				<tr>
					<td align="right" class="viewPropTitle">
						<div id="Pfinal1"><?php echo Translate::g(22); ?>&nbsp;:</div>
					</td>
					<td class="viewProp">
						<div id="Pfinal2">
							<textarea name="FunctFinalCode" cols="<?php echo TEXTAREA_NB_COLS; ?>" rows="<?php echo TEXAREA_NB_ROWS; ?>"><?php echo htmlentities($FunctProp['funct_final_code'], ENT_NOQUOTES, Translate::encoding()); ?></textarea>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right" class="viewPropTitle"><?php echo Translate::g(23); ?>&nbsp;:</td>
					<td class="viewProp"><input type="text" class="text" name="FunctNumArgs" value="<?php echo $FunctProp['funct_num_args']; ?>"></td>
				</tr>
				<tr>
					<td align="right" class="viewPropTitle">&nbsp;</td>
					<td class="viewProp">
						<input type="checkbox" name="FunctAttribAll" id="FunctAttribAll1" value="1"<?php if($attribAll) : ?> checked="checked"<?php endif; ?> />
						<label for="FunctAttribAll1"><?php echo Translate::g(24); ?></label>
					</td>
				</tr>
			</table>
			<input type="hidden" name="function" value="<?php echo $this->function; ?>" />
<?php if($FunctProp['id']) : ?>
			<input type="hidden" name="id" value="<?php echo $FunctProp['id']; ?>" />
<?php endif; ?>
			<input type="hidden" name="action" value="save" />
			<input class="button" type="submit" value="<?php echo Translate::g(51); ?>" />
		</form>
		<script type="text/javascript">ftype();</script>
<?php
	}
}
?>