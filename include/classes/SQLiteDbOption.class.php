<?php
/**
* Web based SQLite management
* Class for manage database options
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: SQLiteDbOption.class.php,v 1.31 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.31 $
*/

class SQLiteDbOption {

	/**
	* reference to the connection object
	*
	* @access public
	* @var object
	*/
	var $connId;

	/**
	* Class constructor
	*
	* @access public
	* @param string $conn reference to the connection object
	*/
	function __construct(&$connId){
		$this->connId = $connId;
		switch($GLOBALS['option_action']){
			case '':
				$this->optionView();
				break;
			case 'check':
				$query = 'PRAGMA integrity_check;';
				$res = $this->connId->getResId($query);
				if($res) $tabRes = $this->connId->getArray();

				$errorMessage = '';
				if(!$res || !$tabRes)
					$errorMessage = Translate::g(9).' : '.@$this->connId->connId->getError."\n";
				displayQuery($queryDisplay);
				if($errorMessage) {
					displayError($errorMessage);
				} else {
					echo '<div style="text-align: center; padding: 10px;">Result : '.$tabRes[0]['integrity_check'].'</div>';
				}
				$this->optionView();
				break;
			case 'vacuum':
				$query = 'VACUUM;';
				$res = $this->connId->getResId($query);
				displayQuery($query);
				$this->optionView();
				break;
			case 'save':
				$query[] = 'PRAGMA '.(($this->connId->connId->getVersion()==3)? '' : 'default_' ).'synchronous='.$_POST['synchro'].';';
				$query[] = 'PRAGMA default_cache_size='.$_POST['cache_size'].';';
				$query[] = 'PRAGMA '.(($this->connId->connId->getVersion()==3)? '' : 'default_' ).'temp_store='.$_POST['temp_store'].';';
				foreach($query as $req) $res = $this->connId->getResId($req);
				$this->optionView();
				break;
		}
	}

	/**
	* Display Available Option
	*
	* @access public
	*/
	function optionView(){
		$dbSel = Request::getInt('dbsel');
		if($tabInfoDb = @stat($this->connId->baseName)){
			if($tabInfoDb['size']<1024) {
				$size = $tabInfoDb['size'].'o';
			} elseif($tabInfoDb['size']< (1024*1024)){
				$size = number_format(($tabInfoDb['size']/1024), 2, ',', '').'Ko';
			} elseif($tabInfoDb['size']< (1024*1024*1024)){
				$size = number_format(($tabInfoDb['size']/(1024*1024)), 2, ',', '').'Mo';
			}
			$perm = substr(sprintf("%o", fileperms($this->connId->baseName)), 3);
			for($i=0 ; $i<3 ; $i++){
				$currPerm = substr($perm, $i, 1);
				$strPerm = '';
				if($currPerm & 4) $strPerm .= 'r'; else $strPerm .= '-';
				if($currPerm & 2) $strPerm .= 'w'; else $strPerm .= '-';
				if($currPerm & 1) $strPerm .= 'x'; else $strPerm .= '-';
				$tabPerm[$i] = $strPerm;
			}
			$perms = $tabPerm[2].$tabPerm[1].$tabPerm[0];
			$dateModif = date('d-m-Y H:i:s', $tabInfoDb['mtime']);
		}
		$res = $this->connId->getResId('PRAGMA '.(($this->connId->connId->getVersion()==3)? '' : 'default_' ).'synchronous;');
		$tabSynchro = $this->connId->getArray();

		if(isset($tabSynchro[0]))
			$valSynchro = $tabSynchro[0]['synchronous']; else $valSynchro = "";
		$res = $this->connId->getResId('PRAGMA cache_size;');
		$tabCache = $this->connId->getArray();
		if(isset($tabCache[0]))
			$valCache = $tabCache[0]['cache_size']; else $valCache = "";
		$res = $this->connId->getResId('PRAGMA '.(($this->connId->connId->getVersion()==3)? '' : 'default_' ).'temp_store;');
		$tabTempStore = $this->connId->getArray();
		if(isset($tabTempStore[0]))
			$valTempStore = $tabTempStore[0]['temp_store']; else $valTempStore = "";
		if(DEMO_MODE)
			$dbLocation = '/***/***/'.basename($this->connId->baseName);
		else
			$dbLocation = $this->connId->baseName;
		$ModifPropOk = (!$this->connId->isReadOnly() && displayCondition('properties'));


		/*
		 * Gestion des base de données attaché
		 */
		$attachAction = Request::getWord('attach_action');
		$attachId = Request::getInt('attachId');
		$db = SQLiteFactory::sqliteGetInstance(SQLiteDb);
		if($attachAction && $attachId){
			switch($attachAction){
				case 'add':
					$query = 'INSERT INTO attachment (base_id, attach_id) VALUES ('.quotes($dbSel).', '.quotes($attachId).');';
					break;
				case 'del':
					$query = 'DELETE FROM attachment WHERE id='.quotes($attachId).';';
					break;
			}
			$db->query($query);
		}
		$tabAttach = SQLiteDbConnect::getAttachDb();
		$tabAttachId = array();
		$query = 'SELECT * FROM database';
		$tabDb = $db->array_query($query, SQLITE_ASSOC);


?>
		<table class="w8 center">
			<tr>
				<td align="center">
					<fieldset>
						<legend><?php echo Translate::g(178); ?></legend>
						<table class="p5">
							<tr>
								<td>&nbsp;</td>
								<td>
									<table>
										<tr bgcolor="#e7dfce">
											<td class="Browse"><span class="infosTitle"><?php echo Translate::g(179); ?></span></td>
											<td class="Browse"><span class="infos"><?php echo $dbLocation; ?></span></td>
										</tr>
										<tr bgcolor="#f7f3ef">
											<td class="Browse"><span class="infosTitle"><?php echo Translate::g(180); ?></span></td>
											<td class="Browse"><span class="infos"><?php echo $size; ?></span></td>
										</tr>
										<tr bgcolor="#e7dfce">
											<td class="Browse"><span class="infosTitle"><?php echo Translate::g(181); ?></span></td>
											<td class="Browse"><span class="infos"><?php echo $perms; ?></span></td>
										</tr>
										<tr bgcolor="#f7f3ef">
											<td class="Browse"><span class="infosTitle"><?php echo Translate::g(182); ?></span></td>
											<td class="Browse"><span class="infos"><?php echo $dateModif; ?></span></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend><?php echo Translate::g(212,'Maintenance'); ?></legend>
						<table class="p5">
							<tr>
								<td>&nbsp;</td>
								<td>
<?php if($ModifPropOk) : ?>
									<a href="main.php?dbsel=<?php echo $dbSel; ?>&amp;action=options&amp;option_action=check" class="base" target="main">&nbsp;&raquo;&nbsp;<?php echo Translate::g(183); ?></a>
									<br/>
									<br/>
<?php else : ?>
									&nbsp;&raquo;&nbsp;<span class="tabprop"><i><?php echo Translate::g(183); ?></i></span>
									<br/>
									<br/>';
<?php endif; ?>
<?php if($ModifPropOk) : ?>
									<a href="main.php?dbsel=<?php echo $dbSel; ?>&amp;action=options&amp;option_action=vacuum" class="base" target="main">&nbsp;&raquo;&nbsp;<?php echo Translate::g(184); ?></a>
<?php else : ?>
									&nbsp;&raquo;&nbsp;<span class="tabprop"><i><?php echo Translate::g(184); ?></i></span>
<?php endif; ?>
								</td>
							</tr>
						</table>
					</fieldset>';
					<fieldset>
						<legend><?php echo Translate::g(84); ?></legend>
						<table class="p10 s5 center">
							<tr>
								<td colspan="2" align="center">
								<form name="formOption" action="main.php?dbsel=<?php echo $dbSel; ?>&amp;action=options&amp;option_action=save" method="post" target="main">
									<table style="font-size: 11px;">
										<tr>
											<td align="right"><?php echo Translate::g(185); ?></td>
											<td align="right">
												<label for="synchro0"><?php echo Translate::g(187); ?></label>
												<input type="radio" name="synchro" id="synchro0" value="0"<?php if($valSynchro==0) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
											<td align="right">
												<label for="synchro1"><?php echo Translate::g(188); ?></label>
												<input type="radio" name="synchro" id="synchro1" value="1"<?php if($valSynchro==1) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
											<td align="right">
												<label for="synchro2"><?php echo Translate::g(189); ?></label>
												<input type="radio" name="synchro" id="synchro2" value="2"<?php if($valSynchro==2) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
										</tr>
										<tr>
											<td align="right"><?php echo Translate::g(186); ?></td>
											<td colspan="3"><input type="text" class="text" name="cache_size" value="<?php echo $valCache; ?>" size="5"<?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> /></td>
										</tr>
										<tr>
											<td align="right"><?php echo Translate::g(193); ?></td>
											<td align="right">
												<label for="temp_store0"><?php echo Translate::g(194); ?></label>
												<input type="radio" name="temp_store" id="temp_store0" value="0"<?php if($valTempStore==0) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
											<td align="right">
												<label for="temp_store1"><?php echo Translate::g(195); ?></label>
												<input type="radio" name="temp_store" id="temp_store1" value="1"<?php if($valTempStore==1) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
											<td align="right">
												<label for="temp_store2"><?php echo Translate::g(196); ?></label>
												<input type="radio" name="temp_store" id="temp_store2" value="2"<?php if($valTempStore==2) : ?> checked="checked"<?php endif; ?><?php if(!$ModifPropOk) : ?> disabled="disabled"<?php endif; ?> />
											</td>
										</tr>
									</table>
									<br>
<?php if($ModifPropOk) : ?>
									<input class="button" type="submit" value="<?php echo Translate::g(200,'Update'); ?>" />
<?php endif; ?>
								</form>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
	<table class="w8 center">
		<tr>
			<td align="center">
				<fieldset>
					<legend><?php echo Translate::g(146); ?></legend>
					<table class="Browse">
						<thead>
							<tr class="Browse">
								<td colspan="2" align="center" class="tapPropTitle"><?php echo Translate::g(131); ?></td>
							</tr>
						</thead>
						<tbody>
<?php
	$idx = 0;
	foreach($tabAttach as $attach_id=>$attachInfo) {
			$tabAttachId[] = $attach_id;
?>
							<tr class="Browse row<?php echo ($idx++%2)?>">
								<td class="Browse"><?php echo $attachInfo['name']; ?></td>
								<td class="Browse" align="center">
									<a href="?dbsel=<?php echo  $dbSel; ?>&amp;action=options&amp;options_action=attach&amp;attach_action=del&amp;attachId=<?php echo $attachInfo['id']; ?>">
										<?php displayPics("supprime.gif", Translate::g(86)); ?>
									</a>
								</td>
							</tr>
<?php } ?>
							<tr class="Browse">
								<td style="padding:5px" colspan="2" align="center">
									<form name="attachDb" method="post">
										<select name="attachId">
											<option value=""><?php echo Translate::g(227); ?></option>
<?php
	foreach($tabDb as $dbInfo) {
			if(!in_array($dbInfo['id'], $tabAttachId) && ($dbInfo['id']!=$dbSel)) {
				$tempDb = SQLiteFactory::sqliteGetInstance($dbInfo["location"]);
				if(is_object($tempDb)) {
					$versionNum = $tempDb->getVersion();
					if($versionNum == $this->connId->connId->getVersion()) {
?>
											<option value="<?php echo $dbInfo['id']; ?>"><?php echo $dbInfo['name']; ?></option>
<?php
					}
				}
			}
	}
?>
										</select>
										<input class="button" type="submit" value="<?php echo Translate::g(43); ?>" />
										<input type="hidden" name="dbsel" value="<?php echo $dbSel; ?>" />
										<input type="hidden" name="action" value="options" />
										<input type="hidden" name="options_action" value="attach" />
										<input type="hidden" name="attach_action" value="add" />
									</form>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
<?php
	}
}
?>