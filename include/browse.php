<?php
/**
* Web based SQLite management
* Show result query with paginate, sort, modify/delete links
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: browse.php,v 1.45 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.45 $
*/

if(!isset($withForm))
	$withForm = true;

if(!isset($DisplayQuery) || empty($DisplayQuery)){
	if($action == 'sql')
		$displayResult = false;

	if(!empty($table) || !empty($view))
		$DisplayQuery = 'SELECT * FROM '.quotes(brackets($table, false).brackets($view, false));
	else
		$DisplayQuery = '';

} else if(!isset($_FILES)) {
	$DisplayQuery = urldecode($GLOBALS['DisplayQuery']);
} elseif( !empty($_POST['DisplayQuery']) || !empty($_GET['DisplayQuery']) ) {
  $DisplayQuery = SQLiteStripSlashes($DisplayQuery);
}

if(!isset($displayResult))
	$displayResult = true;

if(!isset($sql_action))
	$sql_action = '';

if( ($sql_action=='explain') && !preg_match('#EXPLAIN#i', $DisplayQuery) )
	$DisplayQuery = 'EXPLAIN '.$DisplayQuery;

$SQLiteQuery = new sql($workDb, $DisplayQuery);

if( $sql_action != 'modify')
	$error = $SQLiteQuery->verify(false);
else
	$error = false;

if($SQLiteQuery->withReturn && !$error && $displayResult){
	if(!empty($GLOBALS["table"])) $linkItem = 'table='.$GLOBALS["table"];
	else $linkItem = 'view='.$GLOBALS["view"];

	$accessResult = $SQLiteQuery->checkAccessResult($DisplayQuery);

	$DbGrid = new SQLiteToGrid($workDb->connId, $SQLiteQuery->query, 'Browse', true, BROWSE_NB_RECORD_PAGE, '70%');
	$DbGrid->enableSortStyle(false);
	$DbGrid->setGetVars('?dbsel='.$GLOBALS['dbsel'].'&amp;table='.$table.'&amp;action=browseItem&amp;DisplayQuery='.urlencode($DisplayQuery));
	if($DbGrid->getNbRecord()<=BROWSE_NB_RECORD_PAGE)
		$DbGrid->disableNavBarre();
	if($accessResult && (!$workDb->isReadOnly() && displayCondition('data'))){
		if(displayCondition('del'))
			$deleteLink = "<a href=\"main.php?dbsel=".$GLOBALS["dbsel"]."&amp;table=".$accessResult."&amp;action=deleteElement&amp;query=#%QUERY%#&amp;pos=#%POS%#&amp;currentPage=browseItem\" class=\"Browse\" target=\"main\">".displayPics("deleterow.png", Translate::g(15))."</a>";
		else
			$deleteLink = displayPics("deleterow_off.png", Translate::g(15));
		if(displayCondition('data'))
			$modifyLink = "<a href=\"main.php?dbsel=".$GLOBALS["dbsel"]."&amp;table=".$accessResult."&amp;action=modifyElement&amp;query=#%QUERY%#&amp;pos=#%POS%#&amp;currentPage=browseItem\" class=\"Browse\" target=\"main\">".displayPics("edit.png", Translate::g(14))."</a>";
		else
			$modifyLink = displayPics("edit_off.png", Translate::g(14));

		$DbGrid->addCalcColumn(Translate::g(33), '<div class="BrowseImages">'.$modifyLink.'&nbsp;'.$deleteLink.'</div>', 'center', 0);
	}

	$showTime = '<div class="time" align="center">'.Translate::g(213).' '.$SQLiteQuery->queryTime.' '.Translate::g(214).'</div>';
	if(SQLiteFactory::isFullText())
		$caption = '<a href="main.php?dbsel='.$GLOBALS["dbsel"].'&amp;'.$linkItem.'&amp;action=browseItem&amp;fullText=0" target="main">'.displayPics("nofulltext.png", Translate::g(225)).'</a>';
	else
		$caption = '<a href="?dbsel='.$GLOBALS["dbsel"].'&amp;'.$linkItem.'&amp;action=browseItem&amp;fullText=1">'.displayPics("fulltext.png", Translate::g(226)).'</a>';
	if(SQLiteFactory::isHTML())
		$capHTML = '<a href="main.php?dbsel='.$GLOBALS["dbsel"].'&amp;'.$linkItem.'&amp;action=browseItem&amp;HTMLon=0" target="main">'.displayPics("HTML_on.png", "HTML").'</a>';
	else
		$capHTML = '<a href="main.php?dbsel='.$GLOBALS["dbsel"].'&amp;'.$linkItem.'&amp;action=browseItem&amp;HTMLon=1" target="main">'.displayPics("HTML_off.png", "Texte").'</a>';

	$capTable = '<div><div style="float: left">'.$caption.str_repeat('&nbsp;', 3).$capHTML.'</div>'.$showTime.'</div>';

    $DbGrid->build();

	if(!isset($noDisplay) || !$noDisplay)
		displayQuery($DbGrid->getRealQuery());
?>
<?php if($DbGrid->getNbRecord()) : ?>
<table align="center">
	<tr>
		<td><?php echo $capTable; ?></td>
	</tr>
	<tr>
		<td>
			<?php echo $DbGrid->show(); ?>
    		<!-- browse.php -->
			<div class="BrowseOptions">
			<?php if(empty($view) && (!$workDb->isReadOnly() && displayCondition("properties"))): ?>
			<hr width="60%">
			<form name="addView" action="main.php?dbsel=<?php echo $GLOBALS['dbsel']; ?>" method="post" target="main">
				<table class="BrowseOption">
					<tr>
						<td>
							&nbsp;&raquo;&nbsp;<?php echo Translate::g(97); ?>
							<input type="text" class="text" name="ViewName" value="" />
							<?php echo Translate::g(98); ?>
							<input class="button" type="submit" value="<?php echo Translate::g(69); ?>" />
						</td>
					</tr>
				</table>
				<input type="hidden" name="action" value="save" />
				<input type="hidden" name="ViewProp" value="<?php echo urlencode($DisplayQuery); ?>" />
			</form>
			<?php endif; ?>
			<?php if($accessResult && (displayCondition('export'))) : ?>
			<hr width="60%">
			<table class="BrowseOption">
				<tr>
					<td>
						<a href="main.php?dbsel=<?php echo $GLOBALS['dbsel']; ?>&amp;table=<?php echo $GLOBALS['table']; ?>&amp;queryExport=<?php echo urlencode($DisplayQuery); ?>&amp;action=export" class="Browse">&nbsp;&raquo;&nbsp;<?php echo Translate::g(76); ?></a>
					</td>
				</tr>
			</table>
			<?php endif; ?>
			</div>
		</td>
	</tr>
</table>
<?php endif; ?>
<?php
	if(!$DbGrid->getNbRecord())
		$SQLiteQuery->getForm($DbGrid->getRealQuery());

} else {
	if(!$SQLiteQuery->multipleQuery && (!isset($noDisplay) || !$noDisplay))
		displayQuery($DisplayQuery, true, $SQLiteQuery->changesLine);
	else
		$SQLiteQuery->DisplayMultipleResult();

	if(!empty($DisplayQuery) && $error) {
		$withForm = true;
		$errorMessage = "";
		if(is_array($SQLiteQuery->lineError)) $errorMessage = Translate::g(99)." : ".implode(", ", $SQLiteQuery->lineError)."<br>";
		$errorMessage .= $SQLiteQuery->errorMessage;
		displayError($errorMessage);
	}
	if($withForm && WITH_AUTH && !SQLiteFactory::getAuth()->getAccess("execSQL"))
		$withForm = false;
	if($withForm)
		$SQLiteQuery->getForm($DisplayQuery);
}
?>
</body>
</html>
