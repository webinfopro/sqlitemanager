<?php
/**
* Web based SQLite management
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: main.php,v 1.45 2006/04/14 20:18:23 freddy78 Exp $ $Revision: 1.45 $
*/

session_start();
ob_start();
include_once "include/defined.inc.php";
include_once INCLUDE_LIB."config.inc.php";

if(ADVANCED_EDITOR && SQLiteFactory::isHTML() && file_exists(SPAW_PATH."spaw_control.class.php")) {
	$spaw_root = SPAW_PATH;
	$spaw_dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $spaw_root);
	$spaw_base_url = "http://".$_SERVER["HTTP_HOST"].$spaw_dir;
	include_once SPAW_PATH."spaw_control.class.php";
}

if(JSCALENDAR_USE && file_exists(JSCALENDAR_PATH . 'calendar.php')) {
		include_once JSCALENDAR_PATH . 'calendar.php';
		$GlobalCalendar = new DHTML_Calendar(JSCALENDAR_PATH, Translate::l(), 'calendar-win2k-1', false);
}
$dbsel = Request::getInt('dbsel');
$action = Request::getWord('action');
if(!$dbsel && $action != 'auth') {
	displayHeader("main");
?>
<h2 class="sqlmVersion"><?php echo Translate::g(2)." ".SQLiteManagerVersion ?></h2>
	<h4 class="serverInfo"><?php echo Translate::g(3)." ".$SQLiteVersion ?> / <?php echo Translate::g(150)." ".phpversion() ?></h4>
	<?php if(READ_ONLY) echo '<table class="w8 center"><tr><td style="font-size: 10px; border: 1px solid red; color: red; align: center">'.Translate::g(154).'</td></tr></table>'; ?>

	<table align="center" class="home">
		<tr>
			<td class="boxtitle">SQLite
			</td>
			<td class="boxtitlespace">&nbsp;
			</td>
			<td class="boxtitle">SQLiteManager
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap"><?php include INCLUDE_LIB."add_database.php"; ?></td>
			<td class="space">&nbsp;</td>
			<td align="left" style="white-space: nowrap">
				<form action="main.php" method="POST">
					<table class="p10" align="center">
						<tr>
							<td style="white-space: nowrap"><?php echo Translate::g(141); ?> :&nbsp;</td>
							<td>
								<select name="Langue" onChange="submit()">
								<?php
								$lang = Translate::getInstance();
								$listLangue = $lang->langueTranslated;
								natsort($listLangue);
								while(list($lgId, $lgLib) = each($listLangue)) {
								?>
									<option value="<?php echo $lgId; ?>"<?php echo (($lang->currentLangue==$lgId)? ' selected="selected"' : '' ); ?>><?php echo (($listLangue[$lgId])? $listLangue[$lgId] : $lgLib); ?></option>
								<?php
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td style="white-space: nowrap"><?php echo Translate::g(142); ?> :&nbsp;</td>
							<td>
								<select name="Theme" onChange="submit()">
								<?php
								$lang = Translate::getInstance();
								if ($handle = opendir(BASE_DIR . '/theme')) {
									$currentTheme = SQLiteFactory::getTheme();
									while (false !== ($themeId = readdir($handle))) {
										if($themeId=='.' || $themeId=='..')
											continue;
										$themeLib = ($lang->themeTranslated[$themeId])? $lang->themeTranslated[$themeId] : $themeId;
								?>
									<option value="<?php echo $themeId; ?>"<?php echo (($themeId==$currentTheme)? ' selected="selected"' : '' ); ?>><?php echo $themeLib; ?></option>
								<?php
									}
									closedir($handle);
								}
								?>
								</select>
							</td>
						</tr>
					</table>
	        	</form>
				    &nbsp;&raquo;&nbsp;<a href="http://www.sqlite.org/" target="docs" class="Browse"><?php echo Translate::g(4); ?></a>
				<br>&nbsp;&raquo;&nbsp;<a href="http://www.sqlite.org/lang.html" target="docs" class="Browse"><?php echo Translate::g(5); ?></a>
				<br>&nbsp;&raquo;&nbsp;<a href="http://www.sqlitemanager.org" target="docs" class="Browse"><?php echo Translate::g(231); ?></a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="space">&nbsp;</td>
      		<td>
				<?php if(WITH_AUTH) : ?><hr style="border: 1px dashed blue"><?php endif; ?>
				<div style="text-align:left; padding:5px;">
				<?php if(!READ_ONLY && WITH_AUTH && SQLiteFactory::getAuth()->isAdmin()) : ?>
				&raquo;&nbsp;<a href="?action=auth" class="Browse"><?php echo Translate::g(156); ?></a>
				<?php endif; ?>
				<?php if(!READ_ONLY && WITH_AUTH && ALLOW_CHANGE_PASSWD) : ?>
				<?php if($action != "passwd") : ?>
				<br>&raquo;&nbsp;<a href="main.php?action=passwd" class="Browse"><?php echo Translate::g(157); ?></a><br><br>
				<?php else : ?>
				<blockquote>
				<?php
					$authO = new SQLiteAuthProperties();
					$authO->changePasswd();
				?>
				</blockquote>
				<?php endif; ?>
				<?php endif; ?>
				<?php if(WITH_AUTH) : ?>
				&raquo;&nbsp;<a href="index.php?action=logout" target="_parent" class="Browse"><?php echo Translate::g(158); ?></a>
				<?php endif;  ?>
				</div>
			</td>
		</tr>
	</table>
<?php
} else {
	// gestion de la base selectionné
	$workDb = SQLiteDbConnect::getSelected();

	displayHeader("main");

	if($workDb) {
		// If action=='auth', no db selected
		displayMenuTitle();
		$workDb->includeUDF();

		if(ALLOW_FULLSEARCH){
			$sqlite_fts = new sqlite_fulltextsearchex ();
			$sqlite_fts->register ($workDb->connId);
			$sqlite_fts->use_against_cache = true;
		}
	}

	switch($action){
		case '':
		case 'properties':
		default:
			if($workDb->isReadable()) {
				if($table || $TableName) $fileProp = 'tableproperties';
				elseif($view || $ViewName || isset($_POST['ViewName'])) $fileProp = 'viewproperties';
				elseif($trigger || $TriggerName) $fileProp = 'triggerproperties';
				elseif($function) $fileProp = 'functproperties';
				else $fileProp = 'dbproperties';
				if (isset($GLOBALS['plugin'])) {
					include_once 'plugins/'.$GLOBALS['plugin'].'/'.(isset($GLOBALS['file'])?$GLOBALS['file']:'plugin').'.php';
					if (isset($GLOBALS["action"]) && function_exists('plugin_'.$GLOBALS["action"])) {
					$function = 'plugin_'.$GLOBALS["action"];
					$function();
					break;
					}
				}
				include_once INCLUDE_LIB.$fileProp.'.php';
			}
			break;
		case 'browseItem':
			if($workDb->isReadable()) include INCLUDE_LIB.'browse.php';
			break;
		case 'sql':
			if($workDb->isReadable()) {
				include INCLUDE_LIB.'sql.php';
				if ($_SERVER['REQUEST_METHOD'] == 'POST')
				  echo "<script  type=\"text/javascript\">parent.left.location='left.php?dbsel=".$GLOBALS["dbsel"]."';</script>";
			}
			break;
		case "export":
			if($workDb->isReadable()) {
				$export = new SQLiteExport($workDb);
			}
			break;
		case "del":
			$query = "DELETE FROM database WHERE id=".$dbsel.";";
			if($dbsel) {
				$db->query($query);
				// Remove attached databases
				$db->query("DELETE FROM attachment WHERE base_id=".$dbsel." OR attach_id=".$dbsel.";");
			}
			$redirect = "<script  type=\"text/javascript\">parent.location='index.php';</script>";
			break;
		case "add_view":
			if($workDb->isReadable()) {
				$action = "add";
				include_once INCLUDE_LIB."viewproperties.php";
			}
			break;
		case "add_trigger":
			if($workDb->isReadable()) {
				$action = "add";
				include_once INCLUDE_LIB."triggerproperties.php";
			}
			break;
		case "add_function":
			if($workDb->isReadable()) {
				$action = "add";
				include_once INCLUDE_LIB."functproperties.php";
			}
			break;
		case "options":
			if($workDb->isReadable()) {
				$Option = new SQLiteDbOption($workDb);
				if ($_SERVER['REQUEST_METHOD'] == 'POST' || (isset($_REQUEST['attach_action']) && $_REQUEST['attach_action']=='del'))
				  echo "<script  type=\"text/javascript\">parent.left.location='left.php?dbsel=".$dbsel."';</script>";
			}
			break;
		case 'auth':
			$authO = new SQLiteAuthProperties();
			$authO->manageAuth();
			break;
	}

}
syslog(LOG_INFO, "MAIN before redirect" . $redirect);
if(isset($redirect) && !empty($redirect)){
        ob_end_clean();
        echo $redirect;
}
@ob_end_flush();
?>
