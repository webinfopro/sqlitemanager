<?php
/**
* Web based SQLite management
* Some Theme defines
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: define.php,v 1.3 2005/06/05 17:58:37 freddy78 Exp $ $Revision: 1.3 $
*/

$QueryResultAlign			= "center";
$displayQueryTitleColor = "#D3DCE3";
$displayQueryBgColor 	= "#CCCCCC";

$browseColor1				= "#EEEEEE";
$browseColor2				= "#DDDDDD";
$browseColorOver			= "#CCFFCC";
$browseColorClick			= "#FFCC99";

//$theme_author  = '<br/>Frédéric HENNINOT';
//$theme_contact = 'mailto:fhenninot at freesurf.fr';
?>
