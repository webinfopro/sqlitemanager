<?php
/**
* Web based SQLite management
* Show navigation into databases
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: left.php,v 1.35 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.35 $
*/

include_once "include/defined.inc.php";
include_once INCLUDE_LIB."config.inc.php";

?>
<!doctype html>
<html>
<head>
	<title><?php echo Translate::g(3)." ".$GLOBALS['SQLiteVersion'] ?></title>
	<meta http-equiv="content-type" content="text/html;charset=<?php echo Translate::encoding(); ?>">
	<style type="text/css">
		div.logo { width: 100%; background: white; padding-top: 5px; padding-bottom: 5px; }
		div.design { position:absolute; width: 98%; top:47px; text-align:right; color:Silver; font-size:7px; }
	</style>
	<link href="theme/<?php echo SQLiteFactory::getTheme(); ?>/left.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div align="center" class="logo"><?php echo displayPics("sqlitemanager.png","SQLiteManager")?></div>
	<div class="base" align="center" style="margin-top: 5px;">
	<a href="index.php" class="base" target="_parent"><?php echo  Translate::g(1); ?></a>
	</div>
	<table class="main" width="100%">
<?php
$query = "SELECT * FROM database ORDER BY name";
$tabDb = $db->array_query($query, SQLITE_ASSOC);
if(is_array($tabDb)) {
	$dbSel = Request::getInt('dbsel');
	foreach($tabDb as $ligne){
		// get Database version
		$versionNum = sqlite::getDbVersion($ligne["location"]);
		$dbPics = 'database';
		if(in_array($ligne['id'], $attachDbList)) {
			$dbPics .= '_link';
		} else {
			$dbPics .= $versionNum;
		}
		$dbPics .= '.png';
		if(isset($tempDb) && is_object($tempDb)) {
			if($tempDb->dbVersion == 2) $tempDb->close();
			else $tempDb = null;
		}
?>
	<tr class="database" style="vertical-align: middle">
		<td class="img_db" width="18"><?php echo displayPics($dbPics, '', 0, 20); ?></td>
		<td class="name_db"><a href="index.php?dbsel=<?php echo $ligne['id']; ?>" target="_parent" class="dbsel"><?php echo $ligne['name']; ?></a></td>
	</tr>
<?php
		if($ligne['id']==$dbSel){
			$workDb = SQLiteDbConnect::getSelected();

			if(is_resource($workDb->connId) || is_object($workDb->connId)) {
?>
	<tr valign="middle">
		<td colspan="2" class="objects" align="right">
			<table class="items" width="<?php echo (LEFT_FRAME_WIDTH-25); ?>">
<?php
				foreach(SQLiteFactory::$dbItems as $item){
					$list = $workDb->getPropList($item);
					if(is_array($list) && count($list)) {
						foreach($list as $Name) {
							$actionLink = 'main.php?dbsel='.$dbSel.'&amp;'.strtolower($item).'='.urlencode($Name);
?>
				<tr>
					<td class="image" style="white-space: nowrap">
						<a href="<?php echo $actionLink; ?>" target="main" class="item"><?php echo displayPics(strtolower($item) . 's.png'); ?></a>
					<?php if(($item!='Function') && ($item!='Trigger')) : ?>
						<a href="<?php echo $actionLink; ?>&amp;action=browseItem" target="main" class="item"><?php echo displayPics('browse.png', '', 0, 10); ?></a>
					<?php else : ?>
						<?php echo displayPics('nobrowse.png'); ?>
					<?php endif; ?>
					</td>
					<td class="item" style="white-space: nowrap"><a href="<?php echo $actionLink; ?>" target="main" class="item"><?php  echo $Name; ?></a></td>
				</tr>
<?php
						}
					} elseif(DISPLAY_EMPTY_ITEM_LEFT) {
						$actionLink = 'main.php?dbsel='.$dbSel.'&amp;action=add_'.strtolower($item);
?>
				<tr>
					<td class="image" style="white-space: nowrap" width="35">
						<a href="<?php echo $actionLink; ?>" target="main" class="item"><?php echo displayPics(strtolower($item) . 's.png'); ?></a>
						<?php  echo displayPics('nobrowse.png'); ?>
					</td>
					<td class="<?php echo strtolower($item); ?>" style="white-space: nowrap">
						<?php if(!$workDb->isReadOnly() && displayCondition('properties')) : ?>
						<a href="<?php echo $actionLink; ?>" target="main" class="item">+ <?php echo Translate::getInstance()->itemTranslated[$item]; ?></a>
						<?php else : ?>
						<span class="item"><i>+ <?php echo Translate::getInstance()->itemTranslated[$item]; ?></i></span>
						<?php endif; ?>
					</td>
				</tr>
<?php
					}
				}
?>
			</table>
		</td>
	</tr>
<?php
			}
		}
	}
}

?>
</table>
<br/>
<?php if(isset($theme_author)):?>
<div class="design">Theme designed by <?php echo $theme_author?></div>
<?php endif;?>
</body>
</html>