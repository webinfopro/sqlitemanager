<?php 
// ================================================
// SPAW PHP WYSIWYG editor control
// ================================================
// Russian language file
// ================================================
// Developed: Alan Mendelevich, alan@solmetra.lt
// Copyright: Solmetra (c)2003 All rights reserved.
// ------------------------------------------------
//                                www.solmetra.com
// ================================================
// v.1.0, 2003-04-10
// ================================================

// charset to be used in dialogs
$spaw_lang_charset = 'windows-1251';

// language text data array
// first dimension - block, second - exact phrase
// alternative text for toolbar buttons and title for dropdowns - 'title'

$spaw_lang_data = array(
  'cut' => array(
    'title' => 'Âèð³çàòè'
  ),
  'copy' => array(
    'title' => 'Êîï³þâàòè'
  ),
  'paste' => array(
    'title' => 'Âñòàâèòè'
  ),
  'undo' => array(
    'title' => 'Â³äì³íèòè'
  ),
  'redo' => array(
    'title' => 'Ïîâòîðèòè'
  ),
  'image_insert' => array(
    'title' => 'Âñòàâèòè çîáðàæåííÿ',
    'select' => 'Âñòàâèòè',
	'delete' => 'Ñòåðòè', // new 1.0.5
    'cancel' => 'Â³äì³íèòè',
    'library' => 'Á³áë³îòåêà',
    'preview' => 'Ïåðåãëÿä',
    'images' => 'Çîáðàæåííÿ',
    'upload' => 'Çàâàíòàæèòè çîáðàæåííÿ',
    'upload_button' => 'Çàâàíòàæèòè',
    'error' => 'Ïîìèëêà',
    'error_no_image' => 'Âèáåð³òü çîáðàæåííÿ',
    'error_uploading' => 'Ï³ä ÷àñ çàâàíòàæåííÿ âèíèêëà ïîìèëêà. Ñïðîáóéòå ùå ðàç.',
    'error_wrong_type' => 'Íåâ³ðíèé òèï çîáðàæåííÿ',
    'error_no_dir' => 'Á³áë³îòåêà íå ³ñíóº',
	'error_cant_delete' => 'Ñòåðòè íå âäàëîñÿ', // new 1.0.5
  ),
  'image_prop' => array(
    'title' => 'Ïàðàìåòðè çîáðàæåííÿ',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
    'source' => 'Äæåðåëî',
    'alt' => 'Êîðîòêèé îïèñ',
    'align' => 'Âèð³âíþâàííÿ',
    'left' => 'çë³âà (left)',
    'right' => 'ñïðàâà (right)',
    'top' => 'çâåðõó (top)',
    'middle' => 'â öåíòð³ (middle)',
    'bottom' => 'çíèçó (bottom)',
    'absmiddle' => 'àáñ. öåíòð (absmiddle)',
    'texttop' => 'çâåðõó (texttop)',
    'baseline' => 'çíèçó (baseline)',
    'width' => 'Øèðèíà',
    'height' => 'Âèñîòà',
    'border' => 'Ðàìêà',
    'hspace' => 'Ãîð. ïîëÿ',
    'vspace' => 'Âåðò. ïîëÿ',
    'error' => 'Ïîìèëêà',
    'error_width_nan' => 'Øèðèíà íå º ÷èñëîì',
    'error_height_nan' => 'Âèñîòà íå º ÷èñëîì',
    'error_border_nan' => 'Ðàìêà íå º ÷èñëîì',
    'error_hspace_nan' => 'Ãîðèçîíòàëüí³ ïîëÿ íå º ÷èñëîì',
    'error_vspace_nan' => 'Âåðòèêàëüí³ ïîëÿ íå º ÷èñëîì',
  ),
  'hr' => array(
    'title' => 'Ãîðèçîíòàëüíà ë³í³ÿ'
  ),
  'table_create' => array(
    'title' => 'Ñòâîðèòè òàáëèöþ'
  ),
  'table_prop' => array(
    'title' => 'Ïàðàìåòðè òàáëèö³',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
    'rows' => 'Ðÿäêè',
    'columns' => 'Ñòîâïö³',
    'css_class' => 'Ñòèëü', // <=== new 1.0.6
    'width' => 'Øèðèíà',
    'height' => 'Âèñîòà',
    'border' => 'Ðàìêà',
    'pixels' => 'ï³êñ.',
    'cellpadding' => 'Â³äñòóï â³ä ðàìêè',
    'cellspacing' => 'Â³äñòàíü ì³æ êîì³ðêàìè',
    'bg_color' => 'Êîë³ð ôîíó',
    'background' => 'Ôîíîâå çîáðàæåííÿ', // <=== new 1.0.6
    'error' => 'Ïîìèëêà',
    'error_rows_nan' => 'Ðÿäêè íå º ÷èñëîì',
    'error_columns_nan' => 'Ñòîâïö³ íå º ÷èñëîì',
    'error_width_nan' => 'Øèðèíà íå º ÷èñëîì',
    'error_height_nan' => 'Âèñîòà íå º ÷èñëîì',
    'error_border_nan' => 'Ðàìêà íå º ÷èñëîì',
    'error_cellpadding_nan' => 'Â³äñòóï â³ä ðàìêè íå º ÷èñëîì',
    'error_cellspacing_nan' => 'Â³äñòàíü ì³æ êîì³ðêàìè íå º ÷èñëîì',
  ),
  'table_cell_prop' => array(
    'title' => 'Ïàðàìåòðè êîì³ðêè',
    'horizontal_align' => 'Ãîðèçîíòàëüíå âèð³âíþâàííÿ',
    'vertical_align' => 'Âåðòèêàëüíå âèð³âíþâàííÿ',
    'width' => 'Øèðèíà',
    'height' => 'Âèñîòà',
    'css_class' => 'Ñòèëü',
    'no_wrap' => 'Áåç ïåðåíîñó',
    'bg_color' => 'Êîë³ð ôîíó',
    'background' => 'Ôîíîâå çîáðàæåííÿ', // <=== new 1.0.6
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
    'left' => 'Çë³âà',
    'center' => 'Â öåíòð³',
    'right' => 'Ñïðàâà',
    'top' => 'Çâåðõó',
    'middle' => 'Â öåíòð³',
    'bottom' => 'Çíèçó',
    'baseline' => 'Áàçîâà ë³í³ÿ òåêñòó',
    'error' => 'Ïîìèëêà',
    'error_width_nan' => 'Øèðèíà íå º ÷èñëîì',
    'error_height_nan' => 'Âèñîòà íå º ÷èñëîì',
  ),
  'table_row_insert' => array(
    'title' => 'Âñòàâèòè ðÿäîê'
  ),
  'table_column_insert' => array(
    'title' => 'Âñòàâèòè ñòîâ÷èê'
  ),
  'table_row_delete' => array(
    'title' => 'Âèëó÷èòè ðÿäîê'
  ),
  'table_column_delete' => array(
    'title' => 'Âèëó÷èòè ñòîâï÷èê'
  ),
  'table_cell_merge_right' => array(
    'title' => 'Îá\'ºäíàòè âïðàâî'
  ),
  'table_cell_merge_down' => array(
    'title' => 'Îá\'ºäíàòè âë³âî'
  ),
  'table_cell_split_horizontal' => array(
    'title' => 'Ðîçä³ëèòè ïî ãîðèçîíòàë³'
  ),
  'table_cell_split_vertical' => array(
    'title' => 'Ðîçä³ëèòè ïî âåðòèêàë³'
  ),
  'style' => array(
    'title' => 'Ñòèëü'
  ),
  'font' => array(
    'title' => 'Øðèôò'
  ),
  'fontsize' => array(
    'title' => 'Ðîçì³ð'
  ),
  'paragraph' => array(
    'title' => 'Àáçàö'
  ),
  'bold' => array(
    'title' => 'Æèðíèé'
  ),
  'italic' => array(
    'title' => 'Êóðñèâ'
  ),
  'underline' => array(
    'title' => 'Ï³äêðåñëåíèé'
  ),
  'ordered_list' => array(
    'title' => 'Âïîðÿäêîâàíèé ñïèñîê'
  ),
  'bulleted_list' => array(
    'title' => 'Íåâïîðÿäêîâàíèé ñïèñîê'
  ),
  'indent' => array(
    'title' => 'Çá³ëüøèòè â³äñòóï'
  ),
  'unindent' => array(
    'title' => 'Çìåíøèòè â³äñòóï'
  ),
  'left' => array(
    'title' => 'Âèð³âíþâàííÿ çë³âà'
  ),
  'center' => array(
    'title' => 'Âèð³âíþâàííÿ ïî öåíòðó'
  ),
  'right' => array(
    'title' => 'Âèð³âíþâàííÿ ñïðàâà'
  ),
  'fore_color' => array(
    'title' => 'Êîë³ð òåêñòó'
  ),
  'bg_color' => array(
    'title' => 'Êîë³ð ôîíó'
  ),
  'design_tab' => array(
    'title' => 'Ïåðåêëþ÷èòèñÿ â ðåæèì ìàêåòóâàííÿ (WYSIWYG)'
  ),
  'html_tab' => array(
    'title' => 'Ïåðåêëþ÷èòèñÿ â ðåæèì ðåäàãóâàííÿ êîäó (HTML)'
  ),
  'colorpicker' => array(
    'title' => 'Âèá³ð êîëüîðó',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
  ),
  'cleanup' => array(
    'title' => '×èñòêà HTML',
    'confirm' => 'Öÿ îïåðàö³ÿ ïðèáåðå âñ³ ñòèë³, øðèôòè ³ íåïîòð³áí³ òåãè ç ïîòî÷íîãî íàïîâíåííÿ ðåäàêòîðà. Ö³ëêîì àáî ÷àñòêîâî âàøå ôîðìàòóâàííÿ ìîæå áóòè âòðà÷åíî.',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
  ),
  'toggle_borders' => array(
    'title' => 'Âêëþ÷èòè ðàìêè',
  ),
  'hyperlink' => array(
    'title' => 'Ë³íê',
    'url' => 'Àäðåñà',
    'name' => 'Èì\'ÿ',
    'target' => 'Â³äêðèòè',
    'title_attr' => 'Íàçâà',
	'a_type' => 'Òèï', // <=== new 1.0.6
	'type_link' => 'Ïîñèëàííÿ', // <=== new 1.0.6
	'type_anchor' => 'ßê³ð', // <=== new 1.0.6
	'type_link2anchor' => 'Ïîñèëàííÿ íà ÿê³ð', // <=== new 1.0.6
	'anchors' => 'ßêîð³', // <=== new 1.0.6
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
  ),
  'hyperlink_targets' => array( // <=== new 1.0.5
  	'_self' => 'â òîìó æ ôðåéì³ (_self)',
	'_blank' => 'â íîâîìó â³êí³(_blank)',
	'_top' => 'íà âñå â³êíî (_top)',
	'_parent' => 'â áàòüê³âñüêîìó ôðåéì³ (_parent)'
  ),
  'table_row_prop' => array(
    'title' => 'Ïàðàìåòðè ðÿäêà',
    'horizontal_align' => 'Ãîðèçîíòàëüíå âèð³âíþâàííÿ',
    'vertical_align' => 'Âåðòèêàëüíå âèð³âíþâàííÿ',
    'css_class' => 'Ñòèëü',
    'no_wrap' => 'Áåç ïåðåíîñó',
    'bg_color' => 'Êîë³ð ôîíó',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
    'left' => 'Çëåâà',
    'center' => 'Â öåíòð³',
    'right' => 'Ñïðàâà',
    'top' => 'Çâåðõó',
    'middle' => 'Â öåíòð³',
    'bottom' => 'Çíèçó',
    'baseline' => 'Áàçîâà ë³í³ÿ òåêñòó',
  ),
  'symbols' => array(
    'title' => 'Ñïåö. ñèìâîëè',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
  ),
  'templates' => array(
    'title' => 'Øàáëîíè',
  ),
  'page_prop' => array(
    'title' => 'Ïàðàìåòðè ñòîð³íêè',
    'title_tag' => 'Çàãîëîâîê',
    'charset' => 'Êîäóâàííÿ',
    'background' => 'Ôîíîâå çîáðàæåííÿ',
    'bgcolor' => 'Êîë³ð ôîíó',
    'text' => 'Êîë³ð òåêñòó',
    'link' => 'Êîë³ð ë³íê³â',
    'vlink' => 'Êîë³ð â³äâ³äàíèõ ë³íê³â',
    'alink' => 'Êîë³ð àêòèâíèõ ë³íê³â',
    'leftmargin' => 'Â³äñòóï çëåâà',
    'topmargin' => 'Â³äñòóï çâåðõó',
    'css_class' => 'Ñòèëü',
    'ok' => 'ÃÎÒÎÂÎ',
    'cancel' => 'Â³äì³íèòè',
  ),
  'preview' => array(
    'title' => 'Ïîïåðåäí³é ïåðåãëÿä',
  ),
  'image_popup' => array(
    'title' => 'Popup çîáðàæåííÿ',
  ),
  'zoom' => array(
    'title' => 'Çá³ëüøåííÿ',
  ),
  'subscript' => array( // <=== new 1.0.7
    'title' => 'Íèæí³é ³íäåêñ',
  ),
  'superscript' => array( // <=== new 1.0.7
    'title' => 'Âåðõí³é ³íäåêñ',
  ),
);
?>